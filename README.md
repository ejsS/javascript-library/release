The latest version of Easy JavaScript Simulations (ejsS) library.

You can use this library in your local installation of JavaScript EJS:

*  Go to the directory of your local installation, for example it could be in /Users/lookang/Public/PublicLawrence/JavaScript_EJS_6.0, depending on where you set your workspace in EJSS editor
*  Look for the EjsS library in the directory \bin\javascript\lib
*  Update-(Copy and Paste) the EjsS library (the 2 files ejsS.v1.min.js and ejsS.v1.max.js in this release) and choose replace.
*  If file size is a big concern, then you only need ejsS.v1.min.js
*  If human readable is a bigger concern, then you only need ejsS.v1.max.js
*  EJSS editor will export ejsS.v1.max.js if present, or else is will be ejsS.v1.min.js


